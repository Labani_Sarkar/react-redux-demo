import {createStore ,applyMiddleware ,combineReducers}  from "redux";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";
import DataReducer from "./data.reducer";


 
const rootReducer = combineReducers({Data:DataReducer});
let store = createStore(rootReducer,composeWithDevTools(applyMiddleware(thunk)));
export {store };