import DataService from "../service/data/Data.service"
import { ActionTypes } from "./actionTypes"

export const  GetListDataOfUsers = () => {
    return (dispatch:any) =>{
    DataService.getListOfUser().then((res)=>{
        dispatch(patchUserData(res));
       
    }).catch((err)=>{
        console.log(err);
    })
}
} 
export const patchUserData=(value:any)=>{
    return {
       
        type: ActionTypes.GET_LIST_DATA_USERS,
        payload:value
    }
}
export const GetListDataOfPosts = () => {
    return (dispatch:any) =>{
    DataService.getlistofPosts().then((res)=>{
        dispatch(patchPostData(res));
 
    }).catch((err)=>{
        console.log(err);

    })
}
   
} 
export const patchPostData=(value:any)=>{
    return {
        type: ActionTypes.GET_LIST_DATA_POSTS,
        payload:value
    }
}
export const UpdateUserName = (value:any) => {
    return {
        type: ActionTypes.UPDATE_USER_NAME,
        payload:value
    }
} 