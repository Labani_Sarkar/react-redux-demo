import request from "./request";
/**
 * @description GET Method
 * @param url API URL | Required
 * @returns Response or Error
 */
export function getAPICall(url:any){
    return request({
        url: url,
        method: 'GET',
        headers:{"accept":"application/json"}
      });

}
/**
 * @description PUT Method
 * @param url API URL | Required
 * @param body Body  | Required
 * @returns Response or Error
 */
export function putAPICall(url:any, body:any){
        return request({
            url:   url,
            method: 'PUT',
            data:  body,
            headers: {"accept":"application/json"},
          });
}
/**
 * @description PATCH Method
 * @param url API URL | Required
 * @param body Body  | Required
 * @returns Response or Error
 */
export function patchAPICall(url:any, body:any){
    return request({
        url:    url,
        method: 'PATCH',
        data:  body,
        headers:{"accept":"application/json"}
      });
    
}
/**
 * @description POST Method
 * @param url API URL | Required
 * @param body Body  | Required
 * @returns Response or Error
 */
export function postAPICall(url:any, body:any){
    return request({
        url:    url,
        method: 'POST',
        data: body,
        headers: {"accept":"application/json"},
      });
}
/**
 * @description DELETE Method
 * @param url API URL | Required
 * @returns Response or Error
 */
export function deleteAPICall(url:any){
    return request({
        url:    url,
        method: 'DELETE',
        headers: {"accept":"application/json"},
      });
}