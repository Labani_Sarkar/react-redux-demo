import { Input,Form, Button, Row, Col, List,Avatar } from 'antd';
import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { GetListDataOfPosts, GetListDataOfUsers, UpdateUserName } from './redux/data.action';

const Home = (props:any) => {
    const [form] = Form.useForm();
    const [listType, setListType]=useState("user");
    useEffect(()=>{
        props.getListdataUsers()
    },[])
    const OnSubmit=(value:any)=>{
        props.updateuserName(value)
        form.resetFields()
    }
    const changeTab=(value:any)=>{
        setListType(value)
        fetchData(value)
    }
    const fetchData=(value:any)=>{
        if(value == 'user'){
            props.getListdataUsers()
            console.log(props.listData)
        }else{
            props.getListdataPosts()
        }
    }
    return (
        <div className="div-content">
        <h1 className="header-content">Home</h1>
        <div>User Name: {props.userName}</div>
        <Form form={form} onFinish={OnSubmit}>
            <Row><Col md={14} style={{marginLeft: "40px"}}>
            <Form.Item name="userName" rules={[{
                required: true,
                message: "Please enter name to update user name."
            }]}>
        <Input type="text" placeholder="Write your name to update" ></Input></Form.Item></Col>
        <Col md={8}><Button type="primary" htmlType="submit">Update</Button></Col></Row></Form>

        <div>
            <Row><Col md={12}>
            <h3 onClick={()=>changeTab('user')}><a>List of User</a></h3></Col><Col md={12}><h3 onClick={()=>changeTab('post')}><a>List of Posts</a></h3></Col></Row>
           {listType == 'user' ? <List
    itemLayout="horizontal"
    dataSource={props.listData}
    renderItem={(item:any) => (
      <List.Item>
        <List.Item.Meta
          avatar={<Avatar style={{marginLeft: "50px"}} src="https://joeschmoe.io/api/v1/random" />}
          title={<a href="https://ant.design">{item?.name}</a>}
          description={<div>Email: {item?.email} | Phone: {item?.phone} | Address: {item?.address?.street},{item?.address?.suite},{item?.address?.city}, {item?.address?.zipcode} | </div>}
        />
      </List.Item>
    )}
  />:<List
  itemLayout="horizontal"
  dataSource={props.listData}
  renderItem={(item:any) => (
    <List.Item>
      <List.Item.Meta
        avatar={<Avatar style={{marginLeft: "50px"}} src="https://joeschmoe.io/api/v1/random" />}
        title={<a href="https://ant.design">{item?.title}</a>}
        description={item?.body}
      />
    </List.Item>
  )}
/> }
        </div>
        </div>
    )


      
}
const mapStateToProps = (state: any) => {
    return {
      userName: state.Data.userName,
      listData: state.Data.listData,

    };
  };
const mapDispatchToProps = (dispatch: any) => {
    return {
      getListdataPosts: () => dispatch(GetListDataOfPosts()),
      getListdataUsers: () => dispatch(GetListDataOfUsers()),
      updateuserName: (value:any) => dispatch(UpdateUserName(value))
    };
  };
export default connect(mapStateToProps, mapDispatchToProps)(Home)
